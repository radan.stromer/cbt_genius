 <?php 
 	@session_start();
 	$id_tq = @$_GET['id_tq'];
	$no = 1;
	$no2 = 1;
	$sql_tq = mysqli_query($db, "SELECT * FROM tb_topik_quiz JOIN tb_mapel ON tb_topik_quiz.id_mapel = tb_mapel.id WHERE id_tq = '$id_tq'") or die ($db->error);
	$data_tq = mysqli_fetch_array($sql_tq);
 ?>
<script>
	var waktunya;
	waktunya = <?php echo $data_tq['waktu_soal']; ?>;
	var waktu;
	var jalan = 0;
	var habis = 0;

	function init(){
	    checkCookie();
	    mulai();
	}
	function keluar(){
	    if(habis==0){
	        setCookie('waktux',waktu,365);
	    }else{
	        setCookie('waktux',0,-1);
	    }
	}
	function mulai(){
	    jam = Math.floor(waktu/3600);
	    sisa = waktu%3600;
	    menit = Math.floor(sisa/60);
	    sisa2 = sisa%60
	    detik = sisa2%60;
	    if(detik<10){
	        detikx = "0"+detik;
	    }else{
	        detikx = detik;
	    }
	    if(menit<10){
	        menitx = "0"+menit;
	    }else{
	        menitx = menit;
	    }
	    if(jam<10){
	        jamx = "0"+jam;
	    }else{
	        jamx = jam;
	    }
	    document.getElementById("divwaktu").innerHTML = jamx+" Jam : "+menitx+" Menit : "+detikx +" Detik";
	    waktu --;
	    if(waktu>0){
	        t = setTimeout("mulai()",1000);
	        jalan = 1;
	    }else{
	        if(jalan==1){
	            clearTimeout(t);
	        }
	        habis = 1;
	        document.getElementById("kirim").click();
	    }
	}
	function selesai(){    
	    if(jalan==1){
	        clearTimeout(t);
	    }
	    habis = 1;
	}
	function getCookie(c_name){
	    if (document.cookie.length>0){
	        c_start=document.cookie.indexOf(c_name + "=");
	        if (c_start!=-1){
	            c_start=c_start + c_name.length+1;
	            c_end=document.cookie.indexOf(";",c_start);
	            if (c_end==-1) c_end=document.cookie.length;
	            return unescape(document.cookie.substring(c_start,c_end));
	        }
	    }
	    return "";
	}
	function setCookie(c_name,value,expiredays){
	    var exdate=new Date();
	    exdate.setDate(exdate.getDate()+expiredays);
	    document.cookie=c_name+ "=" +escape(value)+((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
	}
	function checkCookie(){
	    waktuy=getCookie('waktux');
	    if (waktuy!=null && waktuy!=""){
	        waktu = waktuy;
	    }else{
	        waktu = waktunya;
	        setCookie('waktux',waktunya,7);
	    }
	}
</script>
<script type="text/javascript">
    window.history.forward();
    function noBack(){ window.history.forward(); }
</script>
<?php 
	if(@$_SESSION['siswa']) : 
 		include "template/header.php";
?>
		<!DOCTYPE html>
		<html>
		<head>
		    <meta charset="utf-8" />
		    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		    <title>Login CBT</title>
		    <link href="style/assets/css/bootstrap.css" rel="stylesheet" />
		    <link href="style/assets/css/font-awesome.css" rel="stylesheet" />
		    <link href="style/assets/css/alaska.css?v=1.2" rel="stylesheet" />
		</head>
		<body onload="init(),noBack();" onpageshow="if (event.persisted) noBack();" onunload="keluar()">
			 <nav class="navbar navbar-default navbar-fixed-top">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <span class="navbar-brand" href="#">
			      	SOAL NO <span class="label label-primary">12</span>
			      </span>
			    </div>
			     <div class="navbar-text navbar-right">
			     	<span class="label label-default">Sisa waktu</span>
			     	<span class="label label-primary" id="divwaktu"></span>
			     </div>
			  </div><!-- /.container-fluid -->
			</nav>
			<div class="container">
				<div id="container-soal" class="col-md-8">
					<form action="inc/proses_soal.php" method="post">
						<?php
	                    $sql_soal_sudah = mysqli_query($db, "SELECT 
	                                                            tb_jawaban_pilgan_temp.id_soal,
	                                                            tb_jawaban_pilgan_temp.jawaban,
	                                                            tb_soal_pilgan.level_group
	                                                         FROM tb_jawaban_pilgan_temp 
	                                                         JOIN tb_soal_pilgan ON tb_soal_pilgan.id_pilgan = tb_jawaban_pilgan_temp.id_soal
	                                                         WHERE tb_jawaban_pilgan_temp.id_peserta = '".$_SESSION['siswa']."' AND tb_jawaban_pilgan_temp.id_tq = '{$id_tq}'") or die ($db->error);
	                    $no_sudah="";
	                    $group_sudah = "";
	                    $n=1;
	                    while($soal_sudah = mysqli_fetch_assoc($sql_soal_sudah)) {
	                        if($n>1){
	                            $no_sudah .= ","; 
	                            $group_sudah .= ",";
	                        }       
	                        $no_sudah .= $soal_sudah["id_soal"];
	                        $group_sudah .= $soal_sudah["level_group"];
	                        $jawaban = $soal_sudah["jawaban"];
	                        $n++;
	                    }
	                    if(!empty($no_sudah)){
	                        if(isset($_GET['revisi_soal'])){
	                        $sql_soal_pilgan = mysqli_query($db, "SELECT * FROM tb_soal_pilgan WHERE id_tq = '$id_tq' AND id_pilgan='".$_GET['revisi_soal']."' limit 1 ") or die ($db->error);
	                        }else{
	                            $sql_soal_pilgan = mysqli_query($db, "SELECT * FROM tb_soal_pilgan WHERE id_tq = '$id_tq' 
	                                AND id_pilgan NOT IN ({$no_sudah}) 
	                                #AND level_group = '$n'
	                                AND level_group NOT IN ({$group_sudah}) 
	                                ORDER BY rand() limit 1 ") or die ($db->error);
	                        }
	                    }elseif(isset($_GET['revisi_soal'])){
	                        $sql_soal_pilgan = mysqli_query($db, "SELECT * FROM tb_soal_pilgan WHERE id_tq = '$id_tq' AND id_pilgan='".$_GET['revisi_soal']."' limit 1 ") or die ($db->error);
	                    }
	                    else{
	                        $sql_soal_pilgan = mysqli_query($db, "SELECT * FROM tb_soal_pilgan 
	                                                              WHERE id_tq = '$id_tq' 
	                                                             # AND level_group = '$n' 
	                                                              ORDER BY rand() limit 1 ") or die ($db->error);
	                    }
						if(mysqli_num_rows($sql_soal_pilgan) > 0){
                    ?>
					</form>
				</div>
				<div class="col-md-4">
					test
				</div>
			</div>
<?php endif; //if(@$_SESSION['siswa']) line 99?>
<?php include "template/footer.php";?>